/*
Navicat MySQL Data Transfer

Source Server         : wk
Source Server Version : 50722
Source Host           : localhost:3306
Source Database       : sims

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2020-02-28 12:29:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_num` varchar(11) DEFAULT NULL COMMENT '商品编号',
  `goods_name` varchar(255) NOT NULL COMMENT '商品名称',
  `type_id` int(11) DEFAULT NULL,
  `supplier` varchar(255) DEFAULT NULL COMMENT '供应商',
  `gross` double(255,0) DEFAULT NULL COMMENT '商品毛重(g)',
  `num` int(11) DEFAULT NULL COMMENT '在库数量',
  `unit` varchar(255) DEFAULT NULL COMMENT '单位',
  `price` double DEFAULT NULL COMMENT '单价',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of goods
-- ----------------------------
INSERT INTO `goods` VALUES ('1', '990592', '麦维他燕麦饼', '1', '麦维他（McVitie\'s）', '300', '120', '袋', '18.8');
INSERT INTO `goods` VALUES ('2', '8451663', 'Tango威化饼干', '1', 'Tango', '220', '80', '盒', '17.5');
INSERT INTO `goods` VALUES ('3', '84521646', '龙角散无', '1', '日本', '42', '32', '盒', '15');
INSERT INTO `goods` VALUES ('4', '1788122', '白い恋人12枚', '1', '白い恋人', '200', '1', '盒', '79');
INSERT INTO `goods` VALUES ('5', '7176975', '草香蕉脆片', '1', '道吉草', '60', '21', '袋', '9.7');
INSERT INTO `goods` VALUES ('6', '4396939', '斯（Totaste） 混合蔬菜味棒形饼干', '1', 'Totaste', '128', '32', '袋', '8.6');
INSERT INTO `goods` VALUES ('7', '4825006', '乐事零食大礼包', '2', ' 乐事（Lay\'s）', '1000', '43', '袋', '67');
INSERT INTO `goods` VALUES ('8', '1231906', '百草味 原切牛肉条', '2', '百草味', '60', '54', '袋', '24.9');
INSERT INTO `goods` VALUES ('9', '3706179', '卫龙小面筋', '2', ' 卫龙', '501', '23', '袋', '15.9');
INSERT INTO `goods` VALUES ('10', '56975693605', '刺猬阿甘 网红山药脆片 ', '2', '刺猬阿甘', '500', '423', '袋', '59.9');
INSERT INTO `goods` VALUES ('11', '1381488', '口水娃炒米3口味', '2', '口水娃', '400', '43', '袋', '8.8');
INSERT INTO `goods` VALUES ('12', '5242972', '爱尚虾条', '2', '爱尚', '390', '43', '袋', '9.8');
INSERT INTO `goods` VALUES ('13', '4962368', '鸡蛋味蛋卷', '2', '波力（POLI）', '500', '32', '盒', '42.8');
INSERT INTO `goods` VALUES ('14', '5255426', '雪饼仙贝组合装', '2', '旺旺', '720', '12', '袋', '19.9');
INSERT INTO `goods` VALUES ('15', '49623682', '青豌豆 ', '2', '甘源青豆', '720', '43', '袋', '22.1');
INSERT INTO `goods` VALUES ('16', '8081963', '亲亲亲亲果冻2400g礼盒', '2', '亲亲', '720', '12', '盒', '19.9');
INSERT INTO `goods` VALUES ('17', '5443740', '琥珀牛肉味锅巴', '2', ' 琥珀', '720', '54', '袋', '9.9');
INSERT INTO `goods` VALUES ('18', '7776792', '三只松鼠乳酸菌面包', '3', '三只松鼠（Three Squirrels）', '720', '645', '袋', '12.1');
INSERT INTO `goods` VALUES ('19', '13038848047', '手撕面包', '3', '三只松鼠（Three Squirrels）', '720', '23', '袋', '43.5');
INSERT INTO `goods` VALUES ('20', '4438311', '三只松鼠蔓越莓曲奇', '3', '三只松鼠（Three Squirrels）', '720', '12', '盒', '12.7');
INSERT INTO `goods` VALUES ('21', '454406021', '良品铺子手撕面包', '3', ' 良品铺子', '720', '43', '盒', '12.9');
INSERT INTO `goods` VALUES ('22', '454406032', '百草味肉松饼1000g', '3', '百草味', '112', '2', '盒', '12.5');
INSERT INTO `goods` VALUES ('23', '454406012', '良品铺子非夹心饼干', '3', '良品铺子', '321', '32', '盒', '43.3');
INSERT INTO `goods` VALUES ('24', '454406013', '百草味原味 华夫饼1kg', '3', '百草味', '23', '43', '盒', '12.6');
INSERT INTO `goods` VALUES ('25', '454406014', '雀巢巧克力', '3', '雀巢', '4123', '43', '盒', '23.8');
INSERT INTO `goods` VALUES ('26', '45440605', '蛋糕糕点零食大礼包 ', '3', '奥利奥（Oreo）', '1414', '54', '盒', '12.4');
INSERT INTO `goods` VALUES ('27', '454406016', '咸蛋黄酥20枚 ', '3', ' 葡记', '4314', '23', '盒', '40.2');
INSERT INTO `goods` VALUES ('28', '454406017', '三只松鼠奶油味夏威夷果', '4', '三只松鼠', '415', '43', '盒', '123.2');
INSERT INTO `goods` VALUES ('29', '454406018', '百草味 坚果', '4', '百草味', '14', '54', '盒', '32.7');
INSERT INTO `goods` VALUES ('30', '454406019', '坚果礼盒零食礼包', '4', '百草味', '232', '64', '盒', '18.6');
INSERT INTO `goods` VALUES ('31', '454406020', '洽洽多味瓜子葵花籽零食恰恰瓜子', '4', '洽洽', '533', '75', '盒', '21.9');
INSERT INTO `goods` VALUES ('32', '454406021', '洽洽焦糖味瓜子108g', '4', '洽洽', '231', '864', '盒', '23');
INSERT INTO `goods` VALUES ('33', '454406022', '洽洽 坚果炒货 零食坚果每日坚果750g（30日装）洽洽 坚果炒货 零食坚果每日坚果750g（30日装）', '4', '洽洽', '4353', '34', '盒', '22');
INSERT INTO `goods` VALUES ('34', '454406023', '三胖蛋礼盒装1308g', '4', ' 三胖蛋', '42', '23', '盒', '35.5');
INSERT INTO `goods` VALUES ('35', '454406024', '楼兰蜜语干果炒货礼盒', '4', '楼兰蜜语（LOU LAN MI YU）', '124', '32', '盒', '42');
INSERT INTO `goods` VALUES ('36', '454406025', '老街口焦糖瓜子', '4', '老街口', '253', '43', '盒', '32');
INSERT INTO `goods` VALUES ('37', '454406026', '沃隆混合坚果', '4', '沃隆', '143', '63', '盒', '21');
INSERT INTO `goods` VALUES ('38', '454406027', '臻味坚果礼盒', '4', '臻味', '134', '54', '盒', '21');
INSERT INTO `goods` VALUES ('39', '454406028', '三只松鼠芒果干', '5', '三只松鼠', '1343', '65', '盒', '12');
INSERT INTO `goods` VALUES ('40', '454406029', '三只松鼠草莓干', '5', '三只松鼠', '5434', '23', '盒', '21');
INSERT INTO `goods` VALUES ('41', '454406030', '百草味缤纷水果干', '5', '百草味', '134', '53', '盒', '12');
INSERT INTO `goods` VALUES ('42', '454406031', '良品铺子冬枣', '5', '良品铺子', '14', '64', '盒', '423');
INSERT INTO `goods` VALUES ('43', '454406032', '好想你红枣干', '5', '好想你', '123', '86', '盒', '23');
INSERT INTO `goods` VALUES ('44', '454406033', '好想你红枣银耳枸杞', '5', '好想你', '134', '767', '盒', '234');
INSERT INTO `goods` VALUES ('45', '454406034', '华味亨110g正宗话梅', '5', '华味亨', '523', '42', '盒', '234');
INSERT INTO `goods` VALUES ('46', '454406035', '果园老农果园老农山楂', '5', '果园老农', '1345', '54', '盒', '23');
INSERT INTO `goods` VALUES ('47', '454406036', '来伊份无花果冻干25g', '5', '来伊份', '2312', '23', '盒', '432');
INSERT INTO `goods` VALUES ('48', '454406037', '佳宝九制橄榄500g', '5', '佳宝', '2424', '43', '盒', '213');
INSERT INTO `goods` VALUES ('49', '454406038', '汽修工作服', '6', '鸿赫', '524', '32', '件', '123');
INSERT INTO `goods` VALUES ('50', '454406039', '春秋长袖工作服套装', '6', '能盾', '31', '43', '件', '213');
INSERT INTO `goods` VALUES ('51', '454406040', ' 医用白大褂', '6', ' 烈焰战队', '12', '53', '件', '123');
INSERT INTO `goods` VALUES ('52', '454406041', '工装裤加大码棉质', '6', '吉普（JEEP）', '23', '54', '件', '123');
INSERT INTO `goods` VALUES ('53', '454406042', '古莱登（GODLIKE）', '6', ' 大码棉服男冬', '253', '4', '件', '135');
INSERT INTO `goods` VALUES ('54', '454406043', '加绒保暖PU夹克冬季', '7', '海澜之家（HLA）', '5234', '24', '件', '498');
INSERT INTO `goods` VALUES ('55', '454406044', '吉普WGQ7918', '7', '吉普', '2131', '23', '件', '298');
INSERT INTO `goods` VALUES ('56', '454406045', '七匹狼夹克', '7', '七匹狼', '342', '23', '件', '355');
INSERT INTO `goods` VALUES ('57', '454406046', '皮夹克男秋冬加绒厚款', '7', '南极人 ', '145', '434', '件', '213');
INSERT INTO `goods` VALUES ('58', '454406047', '提花棒球领休闲时尚茄克', '7', '南极人 ', '1234', '43', '件', '335');
INSERT INTO `goods` VALUES ('59', '454406048', '秋季新品舒适立领净色花纹', '7', '海澜之家', '1512', '12', '件', '198');
INSERT INTO `goods` VALUES ('60', '454406049', '夹克男2019秋冬时尚开衫', '7', '花花公子', '124', '3', '件', '298');
INSERT INTO `goods` VALUES ('61', '454406050', '加绒牛仔外套男新款潮韩', '7', '花花公子', '152', '23', '件', '169');
INSERT INTO `goods` VALUES ('62', '454406051', '双面夹克男装2019秋季新品', '7', '富贵鸟', '2134', '43', '件', '138');
INSERT INTO `goods` VALUES ('63', '454406052', '魔兽logo卫衣', '8', '海澜之家（HLA）', '34', '53', '件', '258');
INSERT INTO `goods` VALUES ('64', '454406053', '卫衣男潮嘻哈宽松', '8', 'Genanx', '2134', '31', '件', '123');
INSERT INTO `goods` VALUES ('65', '454406054', '经典时尚圆领灯芯绒', '8', 'HLA海澜之家', '1512', '43', '件', '119');
INSERT INTO `goods` VALUES ('66', '454406055', '潮流男装男士卫衣', '8', 'Genanx', '512', '53', '件', '383');
INSERT INTO `goods` VALUES ('67', '454406056', '宽松落肩袖卫衣', '8', '以纯', '512', '64', '件', '139');
INSERT INTO `goods` VALUES ('68', '454406057', '加绒加厚连帽韩版', '8', '稻草人', '5123', '75', '件', '118');
INSERT INTO `goods` VALUES ('69', '454406058', '宽松圆领套头情侣卫衣', '8', '卡宾', '324', '86', '件', '219');
INSERT INTO `goods` VALUES ('70', '454406059', ' 连帽开衫纯色宽松新款', '8', 'Baleno班尼路 ', '234', '13', '件', '149');
INSERT INTO `goods` VALUES ('71', '2314', 'eee213', '2', '1234', '12', '31', 'dd', '21.1');
INSERT INTO `goods` VALUES ('72', 'e', '3', '2', '3', '3', '4', '3', '3');
INSERT INTO `goods` VALUES ('73', '3', 'e', '2', 'e', '21', '1', 'e', '12');

-- ----------------------------
-- Table structure for goods_type
-- ----------------------------
DROP TABLE IF EXISTS `goods_type`;
CREATE TABLE `goods_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `type_name` varchar(255) DEFAULT NULL,
  `type_name_detail` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of goods_type
-- ----------------------------
INSERT INTO `goods_type` VALUES ('1', '1101', '食品类', '进口食品', null);
INSERT INTO `goods_type` VALUES ('2', '1101', '食品类', '休闲食品', null);
INSERT INTO `goods_type` VALUES ('3', '1101', '食品类', '蛋糕饼干', null);
INSERT INTO `goods_type` VALUES ('4', '1101', '食品类', '坚果炒货', null);
INSERT INTO `goods_type` VALUES ('5', '1101', '食品类', '蜜饯果干', null);
INSERT INTO `goods_type` VALUES ('7', '1102', '服装类', '夹克', null);
INSERT INTO `goods_type` VALUES ('8', '1102', '服装类', '卫衣', null);
INSERT INTO `goods_type` VALUES ('9', '1102', '服装类', 'T恤', null);
INSERT INTO `goods_type` VALUES ('10', '1102', '服装类', '西装', null);
INSERT INTO `goods_type` VALUES ('11', '1103', '家用电器类', '电视', null);
INSERT INTO `goods_type` VALUES ('12', '1103', '家用电器类', '空调', null);
INSERT INTO `goods_type` VALUES ('13', '1103', '家用电器类', '洗衣机', null);
INSERT INTO `goods_type` VALUES ('14', '1103', '家用电器类', '冰箱', null);
INSERT INTO `goods_type` VALUES ('15', '1104', '家具类', '沙发', null);
INSERT INTO `goods_type` VALUES ('16', '1104', '家具类', '茶几', null);
INSERT INTO `goods_type` VALUES ('17', '1104', '家具类', '电视柜', null);
INSERT INTO `goods_type` VALUES ('18', '1104', '家具类', '鞋柜', null);

-- ----------------------------
-- Table structure for in_stock
-- ----------------------------
DROP TABLE IF EXISTS `in_stock`;
CREATE TABLE `in_stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) NOT NULL,
  `in_num` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of in_stock
-- ----------------------------

-- ----------------------------
-- Table structure for out_stock
-- ----------------------------
DROP TABLE IF EXISTS `out_stock`;
CREATE TABLE `out_stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) NOT NULL,
  `out_num` int(11) NOT NULL COMMENT '出库数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of out_stock
-- ----------------------------

-- ----------------------------
-- Table structure for staff
-- ----------------------------
DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login_num` varchar(255) NOT NULL,
  `login_pwd` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT '姓名',
  `age` int(11) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `idcard` varchar(255) DEFAULT NULL COMMENT '身份证号',
  `entry` varchar(255) DEFAULT NULL COMMENT '入职时间',
  `note` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of staff
-- ----------------------------
INSERT INTO `staff` VALUES ('1', '112201', '123456', '张三', '27', '男', '13131122334', '', null, null);
INSERT INTO `staff` VALUES ('3', '112203', '1122211', '丽丽', '24', '女', '13131312121', '410123199903101122', '2020-02-03', '');
