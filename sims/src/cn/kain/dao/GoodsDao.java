package cn.kain.dao;

import java.util.List;

import cn.kain.entity.Goods;

/**
 * @author kai
 * @email kain.wong@foxmail.com
 * @Data 2020年1月8日 下午4:32:42
 * @Description TODO
 * 
 */
public interface GoodsDao {

	void addGoods(Goods good);
	
	void deleteGoodsById(int id);
	
	Goods selectGoodsById(int id);
	
	List<Goods> selectAllGoods();
	
	/**
	 * 根据typeId查询商品
	 */
	List<Goods> findGoodsByTypeId(int id);
	
	/**
	 * 通过商品代号和商品名称查询商品
	 * @param goods_num
	 * @param goods_name
	 * @return
	 */
	List<Goods> findGoodsByNumAndName(String goods_num,String goods_name);
}
