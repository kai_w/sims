package cn.kain.dao;

import java.util.List;

import cn.kain.entity.GoodsType;

/**
 * @author kai
 * @email kain.wong@foxmail.com
 * @Data 2020年1月7日 下午5:47:15
 * @Description TODO
 * 
 */
public interface GoodsTypeDao {

	/**
	 * typeId若不在数据库中则获取所有的数据， 若存在，则根据typeId获取对应的数据
	 * @param typeId
	 * @return
	 */
	List<GoodsType> getAllType(Integer typeId);

	GoodsType selectById(int id);

	void addType(GoodsType t);

	void deleteType(int id);

	/**
	 * 获取所有的类型名称
	 * @return
	 */
	List<String> getAllTypeName();
	
	/**
	 * 根据类型名称获取第二级名称
	 */
	List<String> getSecondTypeName(String typeName);
	
	/**
	 * 根据1,2级类型名称获取typeid
	 */
	int getTypeId(String typeName,String typeName2);
}
