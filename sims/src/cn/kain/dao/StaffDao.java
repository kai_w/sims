package cn.kain.dao;

import java.util.List;

import cn.kain.entity.Staff;

/**
 * @author kai
 * @email kain.wong@foxmail.com
 * @Data 2020年1月3日 下午11:40:28
 * @Description 员工操作DAO
 * 
 */
public interface StaffDao {

	List<Staff> getAllStaff();

	Staff login(String num, String pwd);

	Staff selectById(int id);

	void addStaff(Staff s);

	void updateStaff(Staff s);

	void deleteStaff(int id);
}
