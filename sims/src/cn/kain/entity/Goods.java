package cn.kain.entity;

import java.io.Serializable;

public class Goods implements Serializable {
    private Integer id;
    
    private String goodsNum;
    
    private String goodsName;

    private Integer typeId;

    private String supplier;

    private Double gross;

    private String unit;	
    
    private Integer num;

    private Double price;

    private static final long serialVersionUID = 1L;

    
    
    public Goods(String goodsNum, String goodsName, Integer typeId, String supplier, Double gross,
			String unit, Integer num, Double price) {
		super();
		this.goodsNum = goodsNum;
		this.goodsName = goodsName;
		this.typeId = typeId;
		this.supplier = supplier;
		this.gross = gross;
		this.unit = unit;
		this.num = num;
		this.price = price;
	}
    
    

	public Goods() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    
    public String getGoodsNum() {
		return goodsNum;
	}

	public void setGoodsNum(String goodsNum) {
		this.goodsNum = goodsNum;
	}

	public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName == null ? null : goodsName.trim();
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier == null ? null : supplier.trim();
    }

    public Double getGross() {
        return gross;
    }

    public void setGross(Double gross) {
        this.gross = gross;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Goods [id=" + id + ", goodsNum=" + goodsNum + ", goodsName=" + goodsName + ", typeId=" + typeId
				+ ", supplier=" + supplier + ", gross=" + gross + ", unit=" + unit + ", num=" + num + ", price=" + price
				+ "]";
	}


    
    
}