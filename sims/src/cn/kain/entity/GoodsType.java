package cn.kain.entity;

import java.io.Serializable;

public class GoodsType implements Serializable {
    private Integer id;

    private Integer typeId;

    private String typeName;

    private String typeNameDetail;

    private String note;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName == null ? null : typeName.trim();
    }

    public String getTypeNameDetail() {
        return typeNameDetail;
    }

    public void setTypeNameDetail(String typeNameDetail) {
        this.typeNameDetail = typeNameDetail == null ? null : typeNameDetail.trim();
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

	@Override
	public String toString() {
		return "Type [id=" + id + ", typeId=" + typeId + ", typeName=" + typeName + ", typeNameDetail=" + typeNameDetail
				+ ", note=" + note + "]";
	}
    
    
}