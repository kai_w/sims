package cn.kain.entity;

import java.io.Serializable;

public class Staff implements Serializable {
    private Integer id;

    private String loginNum;

    private String loginPwd;

    private String name;

    private Integer age;

    private String sex;

    private String phone;

    private String idcard;

    private String entry;

    private String note;

    private static final long serialVersionUID = 1L;

    
    
    public Staff() {
		super();
		// TODO Auto-generated constructor stub
	}
    
    

	public Staff(Integer id, String loginNum, String loginPwd, String name, Integer age, String sex, String phone,
			String idcard, String entry, String note) {
		super();
		this.id = id;
		this.loginNum = loginNum;
		this.loginPwd = loginPwd;
		this.name = name;
		this.age = age;
		this.sex = sex;
		this.phone = phone;
		this.idcard = idcard;
		this.entry = entry;
		this.note = note;
	}



	public Staff(String name, Integer age, String sex, String phone,
			String idcard, String entry, String note) {
		this.name = name;
		this.age = age;
		this.sex = sex;
		this.phone = phone;
		this.idcard = idcard;
		this.entry = entry;
		this.note = note;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLoginNum() {
        return loginNum;
    }

    public void setLoginNum(String loginNum) {
        this.loginNum = loginNum == null ? null : loginNum.trim();
    }

    public String getLoginPwd() {
        return loginPwd;
    }

    public void setLoginPwd(String loginPwd) {
        this.loginPwd = loginPwd == null ? null : loginPwd.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard == null ? null : idcard.trim();
    }

    public String getEntry() {
        return entry;
    }

    public void setEntry(String entry) {
        this.entry = entry == null ? null : entry.trim();
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

	@Override
	public String toString() {
		return "Staff [id=" + id + ", loginNum=" + loginNum + ", loginPwd=" + loginPwd + ", name=" + name + ", age="
				+ age + ", sex=" + sex + ", phone=" + phone + ", idcard=" + idcard + ", entry=" + entry + ", note="
				+ note + "]";
	}
    
}