package cn.kain.frame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.security.MessageDigest;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import cn.kain.dao.GoodsDao;
import cn.kain.dao.GoodsTypeDao;
import cn.kain.dao.impl.GoodsDaoImpl;
import cn.kain.dao.impl.GoodsTypeDaoImpl;
import cn.kain.entity.Goods;

/**
 * @author kai
 * @email kain.wong@foxmail.com
 * @Data 2020年2月20日 下午4:43:02
 * @Description 添加 商品
 * 
 */
public class AddGoodsFrame extends JFrame {

	private JLabel label1 = new JLabel("商品代号 : ");
	private JLabel label2 = new JLabel("商品名称 : ");
	private JLabel label3 = new JLabel("生产厂家 : ");
	private JLabel label4 = new JLabel("商品毛重 : ");
	private JLabel label5 = new JLabel("商品数量 : ");
	private JLabel label6 = new JLabel("商品单位 : ");
	private JLabel label7 = new JLabel("商品单价 : ");
	private JLabel label8 = new JLabel("商品分类 : ");
	private JComboBox<String> jComboBox1 = new JComboBox<String>(); // 商品类型
	private JComboBox<String> jComboBox2 = new JComboBox<String>();
	private JTextField jt1 = new JTextField();
	private JTextField jt2 = new JTextField();
	private JTextField jt3 = new JTextField();
	private JTextField jt4 = new JTextField();
	private JTextField jt5 = new JTextField();
	private JTextField jt6 = new JTextField();
	private JTextField jt7 = new JTextField();
	private JButton submitBtn = new JButton("提交");
	private JButton resBtn = new JButton("重置");
	private TabGoodsList tabList = new TabGoodsList();
	private GoodsTypeDao gt = new GoodsTypeDaoImpl();
	private GoodsDao gd = new GoodsDaoImpl();

	public AddGoodsFrame() {
		setLayout(null);
		label1.setBounds(50, 20, 100, 30);
		label2.setBounds(50, 50, 100, 30);
		label3.setBounds(50, 80, 100, 30);
		label4.setBounds(50, 110, 100, 30);
		label5.setBounds(50, 140, 100, 30);
		label6.setBounds(50, 170, 100, 30);
		label7.setBounds(50, 200, 100, 30);
		label8.setBounds(50, 230, 100, 30);
		jt1.setBounds(120, 22, 100, 25);
		jt2.setBounds(120, 52, 100, 25);
		jt3.setBounds(120, 82, 100, 25);
		jt4.setBounds(120, 112, 100, 25);
		jt5.setBounds(120, 142, 100, 25);
		jt6.setBounds(120, 172, 100, 25);
		jt7.setBounds(120, 202, 100, 25);
		submitBtn.setBounds(50, 290, 80, 25);
		resBtn.setBounds(150, 290, 80, 25);
		jComboBox1.setBounds(120, 232, 70, 25);
		jComboBox2.setBounds(200, 232, 70, 25);
		jComboBox1.setModel(new DefaultComboBoxModel(tabList.getComboBoxDate()));
		jComboBox1.addItemListener(new JComBoxItmeListener1());
		jComboBox2.addItemListener(new JComBoxItmeListener2());
		submitBtn.addActionListener(new subminActionListener());
		resBtn.addActionListener(new refreshActionListener());
		add(label1);
		add(label2);
		add(label3);
		add(label4);
		add(label5);
		add(label6);
		add(label7);
		add(label8);
		add(jComboBox1);
		add(jComboBox2);
		add(jt1);
		add(jt2);
		add(jt3);
		add(jt4);
		add(jt5);
		add(jt6);
		add(jt7);
		add(submitBtn);
		add(resBtn);

	}
	
	/**
	 * 重置按钮功能
	 *
	 */
	private class refreshActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			jt1.setText("");
			jt2.setText("");
			jt3.setText("");
			jt4.setText("");
			jt5.setText("");
			jt6.setText("");
			jt7.setText("");
		}
		
	}

	String choose1 = null;
	String choose2 = null;

	/**
	 * 提交
	 */
	private class subminActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			String goodsNum = jt1.getText();
			String goodsName = jt2.getText();
			String supplier = jt3.getText();
			String gross = jt4.getText(); // double
			String num = jt5.getText(); // int
			String unit = jt6.getText();
			String price = jt7.getText();
			if("".equals(gross)||"".equals(num)||"".equals(price)) {
				JOptionPane.showMessageDialog(AddGoodsFrame.this, "有空值!");
				return;
			}
			int typeId = gt.getTypeId(choose1, choose2);
			double gross1 = 0;
			int num1 = 0;
			double price1 = 0;
			try {
				if (Double.valueOf(gross) instanceof Double) {
					gross1 = Double.valueOf(gross);
				}
				if (Integer.valueOf(num) instanceof Integer) {
					num1 = Integer.valueOf(num);
				}
				if (Double.valueOf(price) instanceof Double) {
					price1 = Double.valueOf(price);
				}
				if (typeId == 0) {
					JOptionPane.showMessageDialog(AddGoodsFrame.this, "商品类型选择错误");
				} else {
					Goods g = new Goods(goodsNum, goodsName, typeId, supplier, gross1, unit, num1, price1);
					gd.addGoods(g);// 添加数据到数据库
					AddGoodsFrame.this.dispose();//添加完成后销毁窗口
					tabList.refreshTableData();  //刷新表格数据
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(AddGoodsFrame.this, "数据格式输入有误!");
			}
		}
	}

	public class JComBoxItmeListener1 implements ItemListener {
		@Override
		public void itemStateChanged(ItemEvent e) {
			choose1 = (String) ((JComboBox) e.getSource()).getSelectedItem();
			jComboBox2.setModel(new DefaultComboBoxModel(getComboBoxDate2(choose1)));
			int index = jComboBox2.getSelectedIndex();
			if (index != -1) {
				choose2 = (String) jComboBox2.getSelectedItem();
			}

		}

		/**
		 * 点击一级下拉选刷新二级下拉选目录
		 */
		public String[] getComboBoxDate2(String typeName) {
			List<String> list = gt.getSecondTypeName(typeName);
			String[] data = new String[list.size()];
			int i = 0;
			for (String t : list) {
				data[i] = t;
				i++;
			}
			return data;
		}
	}

	public class JComBoxItmeListener2 implements ItemListener {
		@Override
		public void itemStateChanged(ItemEvent e) {
			choose2 = (String) ((JComboBox) e.getSource()).getSelectedItem();
		}
	}


}
