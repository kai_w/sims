package cn.kain.frame;

import java.awt.Font;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.*;

import cn.kain.entity.Staff;
import cn.kain.service.StaffService;
import cn.kain.service.impl.StaffServiceImpl;
import cn.kain.util.DataValidateUtil;

/**
 * @author kai
 * @email kain.wong@foxmail.com
 * @Data 2020年1月11日 下午7:58:28
 * @Description 添加员工界面
 * 
 */
public class AddStaffFrame extends JFrame{
	
	private String[] labelStr;
	private Font font = new Font("黑体", 1, 26);
	private Font fieldFont = new Font("宋体",1,23);
	private JLabel title = new JLabel("添 加 员 工");
	private ButtonGroup bg = new ButtonGroup();
	private JRadioButton manBtn = new JRadioButton("男",false);
	private JRadioButton womanBtn = new JRadioButton("女",true);
	private SimpleDateFormat sdf  = new SimpleDateFormat("yyyy-MM-dd");
	private JTextField name = new JTextField();
	private JTextField age = new JTextField();
	private JTextField phone = new JTextField();
	private JTextField idcard = new JTextField();
	private JTextField entry = new JTextField();
	private JTextField note = new JTextField();
	private StaffService staffService = new StaffServiceImpl();
	
	private JButton submitBtn = new JButton("提交员工信息");
	
	public AddStaffFrame() {
		this.setLayout(null);
		title.setBounds(120, 20, 300, 40);
		title.setFont(new Font("楷体", 1, 36));
		add(title);
		labelStr = new String[]{"姓名：","年龄：","性别：","手机号：","身份证号：","入职时间：","备注："};
		int row = 70;
		for(int i=0;i<labelStr.length;i++) {
			JLabel label = new JLabel();
			label.setText(labelStr[i]);
			label.setBounds(100, row, 200, 30);
			label.setFont(font);
			this.add(label);
			row+=50;
		}
		name.setBounds(230, 70, 200, 30);
		age.setBounds(230, 120, 200, 30);
		phone.setBounds(230, 220, 200, 30);
		idcard.setBounds(230, 270, 270, 30);
		entry.setBounds(230, 320, 200, 30);
		note.setBounds(230, 370, 200, 30);
		manBtn.setBounds(230, 170, 70, 30);
		womanBtn.setBounds(300, 170, 70, 30);
		
		submitBtn.setBounds(180, 420, 150, 50);
		
		entry.setText(sdf.format(new Date()));
		entry.setEnabled(false);
		
		name.setFont(fieldFont);
		age.setFont(fieldFont);
		phone.setFont(fieldFont);
		idcard.setFont(fieldFont);
		entry.setFont(fieldFont);
		note.setFont(fieldFont);
		
		manBtn.setFont(font);
		womanBtn.setFont(font);
		this.add(name);this.add(age);
		this.add(phone);this.add(idcard);
		this.add(entry);this.add(note);
		this.add(manBtn);this.add(womanBtn);
		this.add(submitBtn);
		bg.add(manBtn);bg.add(womanBtn);
		
		
		submitBtn.addActionListener(new SubmitAction());
	}
	
	
	
	private class SubmitAction implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			String name1 = name.getText();
			String age1 = age.getText();
			String sex1 = manBtn.isSelected()?"男":"女";
			String phone1 = phone.getText();
			String idcard1 = idcard.getText();
			String entry1 = entry.getText();
			String note1 = note.getText();
			
			//先判断是否为空
			if("".equals(name1)||"".equals(age1)||"".equals(phone1)||"".equals(idcard1)) {
				JOptionPane.showMessageDialog(null, "有空值(姓名,年龄,手机号,身份证号不能为空)");
			}else {
				if(!DataValidateUtil.validateInt(23, 45, age1)) {//验证年龄
					JOptionPane.showMessageDialog(null, "年龄必须在23-45岁之间");
				}else if(!DataValidateUtil.validataPhone(phone1)) {//验证手机号
					JOptionPane.showMessageDialog(null, "手机格式不符");
				}else if(!DataValidateUtil.validataIdcard(idcard1)) {//验证身份证号
					JOptionPane.showMessageDialog(null, "身份证号格式不符");
				}else { //验证都通过就操作入库
					Staff s = new Staff(name1,Integer.valueOf(age1),sex1,phone1,idcard1,entry1,note1);
					staffService.addStaff(s);
					System.out.println("添加成功");
					AddStaffFrame.this.dispose();//添加完成释放释放窗口
				}
			}
			
		}
	}
	

}
