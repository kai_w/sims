package cn.kain.frame;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cn.kain.dao.GoodsTypeDao;
import cn.kain.dao.impl.GoodsTypeDaoImpl;
import cn.kain.service.GoodsTypeService;
import cn.kain.service.impl.GoodsTypeServiceImpl;

/**
 * @author kai
 * @email kain.wong@foxmail.com
 * @Data 2020年1月8日 下午4:42:00
 * @Description TODO
 * 
 */
public class AddTypeFrame extends JFrame {

	private JButton addBtn;
	private JButton cancelBtn;
	private JComboBox<String> jComboBox1;
	private JLabel jLabel1;
	private JLabel jLabel2;
	private JLabel jLabel3;
	private JLabel jLabel4;
	private JLabel jLabel5;
	private JPanel panel;
	private JTextField newType;
	private JTextField secondType;
	private JTextField note;
	private Font font = new Font("宋体", Font.PLAIN, 18);

	public AddTypeFrame() {
		setResizable(false);// 控制窗口不可改变大小
		setLayout(null);
		panel = new JPanel();
		setContentPane(panel);
		panel.setLayout(null);
		// 面版加入组件
		jLabel1 = new JLabel("一级分类: ");
		jLabel2 = new JLabel("创建新一级分类: ");
		jLabel3 = new JLabel("二级分类: ");
		jLabel4 = new JLabel("说明：若一级分类中没有可选择，请在下方输入新分类");
		jLabel5 = new JLabel("备 注: ");
		jComboBox1 = new JComboBox<String>();
		newType = new JTextField();
		secondType = new JTextField();
		note = new JTextField();
		addBtn = new JButton("提交添加");
		cancelBtn = new JButton("取 消");
		// 设置各组件位置
		jLabel1.setBounds(120, 40, 120, 35);
		jLabel2.setBounds(70, 100, 200, 35);
		jLabel3.setBounds(120, 160, 120, 35);
		jLabel4.setBounds(70, 70, 320, 35);
		jLabel5.setBounds(150, 220, 320, 35);
		jComboBox1.setBounds(220, 40, 150, 35);
		newType.setBounds(220, 100, 150, 35);
		secondType.setBounds(220, 160, 150, 35);
		note.setBounds(220, 220, 150, 35);
		addBtn.setBounds(120, 280, 120, 35);
		cancelBtn.setBounds(280, 280, 120, 35);
		// 设置下拉选数据来源
		jComboBox1.setModel(new DefaultComboBoxModel(getComboBoxDate()));

		// 设置组件字体
		jLabel1.setFont(font);
		jLabel2.setFont(font);
		jLabel3.setFont(font);
		jLabel5.setFont(font);
		jLabel4.setForeground(Color.red);
		jComboBox1.setFont(font);
		newType.setFont(font);
		secondType.setFont(font);
		addBtn.setFont(font);
		note.setFont(font);
		cancelBtn.setFont(font);
		// 加入所有组件
		panel.add(jLabel1);
		panel.add(jLabel2);
		panel.add(jLabel3);
		panel.add(jLabel4);
		panel.add(jLabel5);
		panel.add(jComboBox1);
		panel.add(newType);
		panel.add(secondType);
		panel.add(addBtn);
		panel.add(note);
		panel.add(cancelBtn);

		// *** 下拉选选择事件
		jComboBox1.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				String index = (String) ((JComboBox) e.getSource()).getItemAt(0);
				String choose = (String) ((JComboBox) e.getSource()).getSelectedItem();
				// 如果选中了存在的一级分类，则下面新输入框不可用。
				if (!index.equals(choose)) {
					newType.setEditable(false);
					newType.setText("已选择");
				} else {
					newType.setEditable(true);
					newType.setText("");
				}
			}
		});
		// ***下拉选事件结束
		
		//添加按钮事件
		addBtn.addActionListener(new AddListener());
		
		//点击取消事件
		cancelBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AddTypeFrame.this.dispose();
			}
		});
		
		
		SwingCreate.frameClosed(this);
	}

	// 点击添加按钮事件
	private class AddListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			GoodsTypeService service = new GoodsTypeServiceImpl();
			String boxStr = (String) jComboBox1.getSelectedItem();
			String typeName = newType.getText();
			String second = secondType.getText();
			String note1 = note.getText();
			if(newType.isEditable()) {
				if (typeName == null || second == null || "".equals(typeName) || "".equals(second)) {
					JOptionPane.showMessageDialog(AddTypeFrame.this, "不能为空！", "警告", JOptionPane.WARNING_MESSAGE);
				}else {
					service.addGoodsType(typeName, second, note1);
					AddTypeFrame.this.dispose();
				}
			}else {
				if(second == null|| "".equals(second)) {
					JOptionPane.showMessageDialog(AddTypeFrame.this, "不能为空！", "警告", JOptionPane.WARNING_MESSAGE);
				}else {
					service.addGoodsType(boxStr, second, note1);
					AddTypeFrame.this.dispose();
				}
			}
		}
	}


	/**
	 * 获取下拉选数据
	 */
	public String[] getComboBoxDate() {
		GoodsTypeDao dao = new GoodsTypeDaoImpl();
		List<String> list = dao.getAllTypeName();
		String[] data = new String[list.size() + 1];
		data[0] = "请选择";
		int i = 1;
		for (String t : list) {
			data[i] = t;
			i++;
		}
		return data;
	}

}
