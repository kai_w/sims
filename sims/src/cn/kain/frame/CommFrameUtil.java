package cn.kain.frame;

import java.awt.Font;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * @author kai
 * @email kain.wong@foxmail.com
 * @Data 2020年1月12日 下午11:47:08
 * @Description 通用组件窗口
 * 
 */
public class CommFrameUtil{

	
	/**
	 * *设置通用表格组件并设置数据来源
	 * *入参：表格, 二维数组，表格标题
	 */
	public static JTable setTable(DefaultTableModel tableModel) {
		JTable jt = new JTable() {
			@Override //重写方法,设置表格第一列和第二列不可编辑
			public boolean isCellEditable(int row, int column) {
				if(column ==0 ||column ==1) {
					return false;
				}else {
					return true;
				}
			}
		};
		jt.getTableHeader().setReorderingAllowed(false); // 设置表头不可拖动
		jt.getTableHeader().setFont(new Font("黑体", Font.BOLD, 18)); //设置表头字体样式
		jt.setFont(new Font("黑体", Font.PLAIN, 16));  //设置表格内部字体样式
		jt.setRowHeight(30); // 设置表格单元格行高 
		jt.setSelectionMode(0); // 设置表格只允许单个选中
		jt.setRowSelectionAllowed(true);// 设置此表可以默认选中 
		jt.setModel(tableModel);//设置数据来源
		return jt;
	}
	
	/**
	 * 设置表格默认选中第一行
	 */
	public static void setDefaultTabelRow(JTable jt,DefaultTableModel tableModel) {
		if(tableModel.getRowCount()!=0) {
			jt.setRowSelectionInterval(0, 0);// 默认选中第一行
		}
	}
}
