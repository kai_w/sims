package cn.kain.frame;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import cn.kain.dao.StaffDao;
import cn.kain.dao.impl.StaffDaoImpl;
import cn.kain.entity.Staff;

/**
 * @author kai
 * @email kain.wong@foxmail.com
 * @Data 2020年1月3日 下午8:11:24
 * @Description 登录窗口
 * 
 */
public class LoginFrame extends JFrame {

	private StaffDao sd = new StaffDaoImpl();
	private JLabel jl_title = new JLabel("超市进销存管理系统"); // 登录窗口标题
	private JLabel jl_num = new JLabel("账  号:"); // 用户名标签
	private JLabel jl_pwd = new JLabel("密  码:"); // 密码标签
	private JTextField jtf_num = new JTextField(); // 用户名
	private JPasswordField jpf_pwd = new JPasswordField(); // 密码输入框
	private JButton login_btn = new JButton("登 录"); // 登录按钮
	private JPanel panel;
	public LoginFrame() {
		this.setResizable(false); //设置窗口不可改变大小
		
		// 设置标题位置和样式
		jl_title.setBounds(90, 20, 400, 30);
		Font font = new Font("宋体", 20, 36);
		jl_title.setFont(font);

		// 设置用户名密码标签位置样式
		Font font2 = new Font("宋体", 20, 23);
		jl_num.setBounds(105, 90, 150, 40);
		jl_pwd.setBounds(105, 140, 150, 40);
		jl_num.setFont(font2);
		jl_pwd.setFont(font2);

		// 设置输入框位置样式
		jtf_num.setBounds(200, 90, 180, 35);
		jpf_pwd.setBounds(200, 145, 180, 35);
		Font font3 = new Font("宋体", 20, 18);
		jtf_num.setFont(font3);
		jpf_pwd.setFont(font3);

		// 设置登录按钮
		login_btn.setBounds(105, 210, 280, 35);
		login_btn.setFont(font3);

		// 将所有组件加到面板中
		panel = new JPanel();
		panel.setLayout(null);
		panel.add(jl_title);
		panel.add(jl_num);
		panel.add(jl_pwd);
		panel.add(jtf_num);
		panel.add(jpf_pwd);
		panel.add(login_btn);
		this.add(panel);
		// 添加监听器(点击登录按钮获取用户名密码)
		login_btn.addActionListener(new loginListener());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	/**
	 * 登录事件监听器
	 */
	private class loginListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String num = jtf_num.getText();
			String pwd = String.valueOf(jpf_pwd.getPassword());
			if (num == null || "".equals(num) || pwd == null || "".equals(pwd)) {
				JOptionPane.showMessageDialog(LoginFrame.this, "账号或密码不能为空！", "登录错误", JOptionPane.WARNING_MESSAGE);
			} else {
				Staff st = sd.login(num, pwd);
				if (st != null && st.getId() != null) { // 登录成功
					LoginFrame.this.dispose(); // 销毁登录窗口（内部类访问外部类）
					SwingCreate.createFrame(new MainFrame(), 1500, 800, "超市进销存管理系统  员工: "+st.getName());
				} else {
					JOptionPane.showMessageDialog(LoginFrame.this, "用户名或密码错误！", "登录错误", JOptionPane.WARNING_MESSAGE);
				}
			}

		}
	}

}
