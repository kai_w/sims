package cn.kain.frame;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * @author kai
 * @email kain.wong@foxmail.com
 * @Data 2020年1月4日 下午10:39:44
 * @Description 用页签的方式创建主窗口
 * 
 */
public class MainFrame extends JFrame{
	
	private String[] menu = {"商品管理","商品类型管理","员工管理","其他"};
	
	private JTabbedPane tabs = new JTabbedPane();
	private JLabel label = new JLabel();
	public MainFrame() {
		tabs.setFont(new Font("黑体", 20, 23));
		label.setFont(new Font("宋体",20,16));
		tabs.add(menu[0],new TabGoodsList());
		tabs.add(menu[1],new TabGoodsType());
		tabs.add(menu[2],new TabManageStaff());
		tabs.add(menu[3],new TabOther());
		add(BorderLayout.SOUTH,label);
		add(tabs);
		tabs.addChangeListener(new TabChangeListener());
		label.setText("面板选中 : "+"商品进入库");//初始化参数
		//setResizable(false);
		
		SwingCreate.SystemClosed(this);
	}
	
	/**
	 * @Description 点击页签改变事件
	 */
	private class TabChangeListener implements ChangeListener {

		public void stateChanged(ChangeEvent e) {
			JTabbedPane str = ((JTabbedPane)e.getSource());
			label.setText("面板选中: "+menu[str.getSelectedIndex()]);
		}
	}
	
	/*
	 * public static void main(String[] args) { SwingCreate.createFrame(new
	 * MainFrame(), 1500, 800, "管理窗口"); }
	 */
	
}

