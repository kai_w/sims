package cn.kain.frame;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import cn.kain.dao.GoodsDao;
import cn.kain.dao.GoodsTypeDao;
import cn.kain.dao.impl.GoodsDaoImpl;
import cn.kain.dao.impl.GoodsTypeDaoImpl;
import cn.kain.entity.Goods;
import cn.kain.entity.Staff;

/**
 * @author kai
 * @email kain.wong@foxmail.com
 * @Data 2020年1月7日 下午5:22:30
 * @Description TODO
 * 
 */
public class TabGoodsList extends JPanel {

	private JLabel label1 = new JLabel("查询条件 : ");
	private JLabel label2 = new JLabel("商品代号:");
	private JTextField goodsIdField = new JTextField();
	private JLabel label3 = new JLabel("商品名称 : ");
	private JTextField goodsNameField = new JTextField();
	private JLabel label4 = new JLabel("商品种类 : ");
	private JComboBox<String> jComboBox1 = new JComboBox<String>();
	private JComboBox<String> jComboBox2 = new JComboBox<String>();
	private JButton queryBtn = new JButton("点击查询");
	private JButton addGoodBtn = new JButton("添加商品");
	
	private JScrollPane scrollPane;
	private JTable jt = new JTable();
	private String[] tableTitle = { "序号", "数据库ID", "商品代号", "商品名称", "厂家", "毛重", "库存数量", "数量单位", "价格(元)" };
	private DefaultTableModel tableModel;
	
	private GoodsDao gd = new GoodsDaoImpl();
	private GoodsTypeDao gt = new GoodsTypeDaoImpl();
	
	public TabGoodsList() {
		this.setLayout(null);
		List<Goods> list = gd.findGoodsByTypeId(1);
		tableModel = new DefaultTableModel(getGoodsData(list), tableTitle);
		jt = CommFrameUtil.setTable(tableModel);
		
		// 数据表格
		scrollPane = new JScrollPane(jt); // 表格放入滚动panel
		scrollPane.setBounds(30, 110, 1300, 500);
		scrollPane.setBackground(Color.DARK_GRAY);

		// 搜索菜单栏
		label1.setFont(new Font("黑体", Font.PLAIN, 20));
		label1.setBounds(50, 20, 130, 30);
		label2.setFont(new Font("黑体", Font.PLAIN, 16));
		label2.setBounds(200, 20, 130, 30);
		goodsIdField.setFont(new Font("黑体", Font.PLAIN, 23));
		goodsIdField.setBounds(270, 20, 130, 30);
		goodsIdField.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
			}
			@Override
			public void focusGained(FocusEvent e) {
				goodsNameField.setText("");
			}
		});
		goodsNameField.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
			}
			@Override
			public void focusGained(FocusEvent e) {
				goodsIdField.setText("");
			}
		});
		
		label3.setFont(new Font("黑体", Font.PLAIN, 16));
		label3.setBounds(420, 20, 100, 30);
		goodsNameField.setFont(new Font("黑体", Font.PLAIN, 16));
		goodsNameField.setBounds(500, 20, 130, 30);

		queryBtn.setFont(new Font("黑体", Font.PLAIN, 16));
		queryBtn.setBounds(650, 20, 100, 30);
		queryBtn.addActionListener(new QueryListener());
		addGoodBtn.setFont(new Font("黑体", Font.PLAIN, 16));
		addGoodBtn.setBounds(780, 20, 100, 30);
		addGoodBtn.setBackground(Color.green);
		addGoodBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingCreate.createFrame(new AddGoodsFrame(), 300, 420, "添加商品");;
			}
		});
		
		
		label4.setFont(new Font("黑体", Font.PLAIN, 16));
		label4.setBounds(200, 60, 100, 30);

		jComboBox1.setFont(new Font("黑体", Font.PLAIN, 16));
		jComboBox1.setBounds(300, 60, 120, 30);
		jComboBox2.setFont(new Font("黑体", Font.PLAIN, 16));
		jComboBox2.setBounds(450, 60, 100, 30);
		// 设置下拉选数据来源
		jComboBox1.setModel(new DefaultComboBoxModel(getComboBoxDate()));
		jComboBox1.addItemListener(new JComBoxItmeListener1());
		jComboBox2.addItemListener(new JComBoxItmeListener2());
		JPanel jp = new JPanel();
		jp.setLayout(null);
		jp.add(label1);
		jp.add(label2);
		jp.add(goodsIdField);
		jp.add(label3);
		jp.add(goodsNameField);
		jp.add(label4);
		jp.add(jComboBox1);
		jp.add(jComboBox2);
		jp.add(queryBtn);
		jp.add(addGoodBtn);
		jp.setBounds(0, 0, 1500, 100);
		jp.setBackground(Color.CYAN);
		add(jp);
		add(scrollPane);
		
		
	}

	/**
	 * 获取商品数据
	 * 
	 * @return
	 */
	private Object[][] getGoodsData(List<Goods> list) {
		Object[][] data = new Object[list.size()][tableTitle.length];
		int i = 0;
		for (Goods g : list) {
			data[i][0] = (i + 1);
			data[i][1] = g.getId();
			data[i][2] = g.getGoodsNum();
			data[i][3] = g.getGoodsName();
			data[i][4] = g.getSupplier();
			data[i][5] = g.getGross();
			data[i][6] = g.getNum();
			data[i][7] = g.getUnit();
			data[i][8] = g.getPrice();
			i++;
		}
		return data;
	}
	
	
	/**
	 * 查询功能
	 */
	private class QueryListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			String goodsNum = goodsIdField.getText();
			String goodsName = goodsNameField.getText();
			List<Goods> goodlist = gd.findGoodsByNumAndName(goodsNum, goodsName);
			tableModel.setDataVector(getGoodsData(goodlist), tableTitle);
		}
	}

	
	
	String choose1 = null;
	String choose2 = null;

	/**
	 * 下拉选1
	 */
	public class JComBoxItmeListener1 implements ItemListener {
		@Override
		public void itemStateChanged(ItemEvent e) {
			choose1 = (String) ((JComboBox) e.getSource()).getSelectedItem();
			jComboBox2.setModel(new DefaultComboBoxModel(getComboBoxDate2(choose1)));
			int index = jComboBox2.getSelectedIndex();
			if(index!=-1) {
				choose2 = (String)jComboBox2.getSelectedItem();
			}
			int typeId = gt.getTypeId(choose1, choose2);
			//System.out.println("已选择:" + choose1 + " " + choose2+" typeId: " + typeId);
			List<Goods> list = gd.findGoodsByTypeId(typeId);
			tableModel.setDataVector(getGoodsData(list), tableTitle);
		}

		/**
		 * 点击一级下拉选刷新二级下拉选目录
		 * 
		 * @return
		 */
		public String[] getComboBoxDate2(String typeName) {
			List<String> list = gt.getSecondTypeName(typeName);
			String[] data = new String[list.size()];
			int i = 0;
			for (String t : list) {
				data[i] = t;
				i++;
			}
			return data;
		}
	}

	/**
	 * 下拉选2
	 */
	public class JComBoxItmeListener2 implements ItemListener {
		@Override
		public void itemStateChanged(ItemEvent e) {
			choose2 = (String) ((JComboBox) e.getSource()).getSelectedItem();
			int typeId = gt.getTypeId(choose1, choose2);
			//System.out.println("已选择:" + choose1 + " " + choose2+" typeId: " + typeId);
			List<Goods> list = gd.findGoodsByTypeId(typeId);
			tableModel.setDataVector(getGoodsData(list), tableTitle);
		}
	}

	/**
	 * @return
	 */
	public String[] getComboBoxDate() {
		List<String> list = gt.getAllTypeName();
		String[] data = new String[list.size() + 1];
		data[0] = "请选择";
		int i = 1;
		for (String t : list) {
			data[i] = t;
			i++;
		}
		return data;
	}
	
	
	public void refreshTableData() {
		List<Goods> list = gd.findGoodsByTypeId(1);
		tableModel = new DefaultTableModel(getGoodsData(list), tableTitle);
	}
}
