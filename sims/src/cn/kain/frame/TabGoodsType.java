package cn.kain.frame;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import cn.kain.dao.GoodsTypeDao;
import cn.kain.dao.impl.GoodsTypeDaoImpl;
import cn.kain.entity.GoodsType;

/**
 * @author kai
 * @email kain.wong@foxmail.com
 * @Data 2020年1月7日 下午5:22:51
 * @Description 商品类型列表
 * 
 */
public class TabGoodsType extends JPanel {

	private JScrollPane scrollPane;
	private JTable jt = null;
	private GoodsTypeDao tdao = new GoodsTypeDaoImpl();
	private List<GoodsType> listType;
	private JButton addBtn = new JButton("添加类型");
	private JButton refreshBtn = new JButton("刷新数据");
	private JButton delBtn = new JButton("删除数据");
	private String[] tableTitle = new String[] { "序号", "数据库ID", "分类ID", "一级分类", "二级分类", "备注" };
	private DefaultTableModel tableModel;

	public TabGoodsType() {
		jt = new JTable();
		jt.getTableHeader().setReorderingAllowed(false);
		 // 设置表头不可拖动
		jt.getTableHeader().setFont(new Font("黑体", Font.BOLD, 18));
		jt.setFont(new Font("黑体", Font.PLAIN, 16));
		jt.setRowHeight(30); // 设置表格单元格行高
		jt.setSelectionMode(0); // 设置表格只允许单个选中

		// 设置数据来源
		
		tableModel = new DefaultTableModel(getDate(), tableTitle);
		//tableModel.isCellEditable(tableTitle.length, listType.size());
		jt.setModel(tableModel);
		jt.setRowSelectionAllowed(true);// 设置此表可以默认选中
		jt.setRowSelectionInterval(0, 0);// 默认选中第一行

		scrollPane = new JScrollPane(jt); // 表格放入滚动panel
		setLayout(new BorderLayout());
		add(BorderLayout.CENTER, scrollPane);

		// 创建第二个panel，用来放置按钮
		JPanel panel = new JPanel();
		panel.setLocation(0, 500);
		panel.setLayout(new FlowLayout());
		addBtn.setFont(new Font("黑体", Font.PLAIN, 16));
		refreshBtn.setFont(new Font("黑体", Font.PLAIN, 16));
		delBtn.setFont(new Font("黑体", Font.PLAIN, 16));
		panel.add(addBtn);
		panel.add(refreshBtn);
		panel.add(delBtn);
		addBtn.addActionListener(new AddTypeListener());
		refreshBtn.addActionListener(new RefreshListener());
		delBtn.addActionListener(new DeleteListener());
		add(BorderLayout.SOUTH, panel);
	}

	/**
	 * 将数据获取转化为二维数组
	 * 
	 * @return
	 */
	public Object[][] getDate() {
		listType = tdao.getAllType(0);
		Object[][] data = new Object[listType.size()][tableTitle.length];
		int i = 0;
		for (GoodsType g : listType) {
			data[i][0] = i + 1;
			data[i][1] = g.getId();
			data[i][2] = g.getTypeId();
			data[i][3] = g.getTypeName();
			data[i][4] = g.getTypeNameDetail();
			data[i][5] = g.getNote();
			i++;
		}
		return data;
	}

	// 点击添加按钮事件
	private class AddTypeListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			SwingCreate.createFrame(new AddTypeFrame(), 500, 430, "添加商品类型");
		}
	}

	// 点击刷新数据按钮事件
	private class RefreshListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			tableModel.setDataVector(getDate(), tableTitle);
			jt.setRowSelectionAllowed(true);// 设置此表可以默认选中
			jt.setRowSelectionInterval(0, 0);// 默认选中第一行
		}
	}

	// 点击删除按钮事件
	private class DeleteListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			//int index = jt.getSelectedRowCount(); // 返回选中的行数
			int index2 = jt.getSelectedRow(); // 返回选定行的索引
			Integer id = (Integer) jt.getValueAt(index2, 1);
			GoodsType type = tdao.selectById(id);
			if (type == null || !id.equals(type.getId())) {
				JOptionPane.showMessageDialog(null, "该行数据已不存在，请刷新数据");
			} else {
				int is = JOptionPane.showConfirmDialog(null, "删除："+type.getTypeName()+" -> "+type.getTypeNameDetail());
				if(is==0) {
					tdao.deleteType(id); //若存在则删除。
				}else {
					System.out.println("不删除");
				}
			}
		}
	}
}
