package cn.kain.frame;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import cn.kain.dao.StaffDao;
import cn.kain.dao.impl.StaffDaoImpl;
import cn.kain.entity.Staff;
import cn.kain.util.DataValidateUtil;

/**
 * @author kai
 * @email kain.wong@foxmail.com
 * @Data 2020年1月7日 下午5:11:41
 * @Description 员工管理界面
 * 
 */
public class TabManageStaff extends JPanel {

	private JPanel addStaffPanel = new JPanel(); // 添加员工
	private JScrollPane listPanel; // 员工列表
	private JTable jt = new JTable();
	private String[] tableTitle = { "序号", "数据库ID", "账号", "密码", "姓名", "年龄", "性别", "手机号", "身份证号", "入职时间", "备注" };

	private StaffDao sDao = new StaffDaoImpl();
	private List<Staff> staffList = new ArrayList<Staff>();

	// 添加员工组件
	private JButton addBtn = new JButton("添加员工");
	private JButton refeshBtn = new JButton("刷新数据");

	private DefaultTableModel tableModel;

	private JPopupMenu popup = new JPopupMenu();
	int row = 0; // 记录鼠标所移动到的行
	int column = 0; // 记录鼠标所移动到的列
	
	public TabManageStaff() {
		setLayout(new BorderLayout());
		addStaffPanel.setLayout(new FlowLayout());
		addStaffPanel.add(addBtn);
		addStaffPanel.add(refeshBtn);
		add(BorderLayout.NORTH, addStaffPanel);

		/**
		 * TODO: 入参改为传入一个list,和一个表格菜单 自动识别list集合类的对象, 并获取对象类的属性值转为二维数组。 后期尝试使用 反射获取
		 */
		tableModel = new DefaultTableModel(getStaffData(), tableTitle);
		jt = CommFrameUtil.setTable(tableModel);
		
		int rowCount = jt.getRowCount();  //表格数据总行数
		for(int i=0;i<rowCount;i++) {
			jt.getCellEditor(i, 1).cancelCellEditing();
		}
		
		CommFrameUtil.setDefaultTabelRow(jt, tableModel);
		listPanel = new JScrollPane(jt);
		add(BorderLayout.CENTER, listPanel);

		addBtn.addActionListener(new AddStaffListener());
		refeshBtn.addActionListener(new refeshStaffListener());

		jt.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				maybeShowPopup(e);
			}

			public void mouseReleased(MouseEvent e) {
				maybeShowPopup(e);
			}

		});

		JMenuItem m = new JMenuItem("删 除");
		m.setFont(new Font("黑体", Font.PLAIN, 16));
		m.addActionListener(new DeleteMenuAction());
		popup.add(m);
		popup.addSeparator(); // 增加分割线
		m = new JMenuItem("修 改");
		m.setFont(new Font("黑体", Font.PLAIN, 16));
		m.addActionListener(new UpdateMenuAction());
		popup.add(m);
	}

	
	/**
	 * 选中所在的对象
	 * @return
	 */
	public Staff getRowStaff() {
		int row = jt.getSelectedRow(); // 选择的行
		Integer id = (Integer) jt.getValueAt(row, 1);
		Staff staff = sDao.selectById(id);
		return staff;
	}
	
	/**
	 * 鼠标选中行右键菜单功能 删除功能
	 */
	private class DeleteMenuAction implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			Staff s = getRowStaff();
			if(s==null) {
				JOptionPane.showMessageDialog(null, "该数据已被删除,请刷新数据.");  //防止界面没刷新成功
			}else {
				int flag = JOptionPane.showConfirmDialog(null, "确认删除 : "+s.getName());
				if(flag==0) {
					sDao.deleteStaff(s.getId());
					tableModel.setDataVector(getStaffData(), tableTitle);  //删除成功刷新数据
					CommFrameUtil.setDefaultTabelRow(jt, tableModel);
				}else {
					System.out.println("取消删除.");
				}
			}
		}
	}

	/**
	 * 鼠标选中行右键菜单功能 修改功能
	 */
	private class UpdateMenuAction implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			int row = jt.getSelectedRow(); // 选择的行
			Integer id = (Integer) jt.getValueAt(row, 1);
			String loginNum = (String) jt.getValueAt(row, 2);
			String loginPwd = (String) jt.getValueAt(row, 3);
			String name = (String) jt.getValueAt(row, 4);
			Integer age = (Integer)jt.getValueAt(row, 5);  //偶尔抽风会报 类转换异常, String or Integer
			if (jt.getValueAt(row, 5)  instanceof String) {
				System.out.println("String");
			}else if(jt.getValueAt(row, 5)  instanceof Integer) {
				System.out.println("Integer");
			}
			String sex = (String) jt.getValueAt(row, 6);
			String phone = (String) jt.getValueAt(row, 7);
			String idcard = (String) jt.getValueAt(row, 8);
			String entry = (String) jt.getValueAt(row, 9);
			String note = (String) jt.getValueAt(row, 10);
			//目前缺少统一验证方法,后续会继续添加
			if("男".equals(sex)||"女".equals(sex)) {
				if(DataValidateUtil.validateInt(23, 45, String.valueOf(age))) {
					Staff s = new Staff(id,loginNum,loginPwd,name,Integer.valueOf(age),sex,phone,idcard,entry,note);
					sDao.updateStaff(s);
					System.out.println("修改成功!");
					//修改成功刷新数据
					tableModel.setDataVector(getStaffData(), tableTitle);
					CommFrameUtil.setDefaultTabelRow(jt, tableModel);
				}else {
					JOptionPane.showMessageDialog(null, "年龄输入有误: 数字且在23-45之间");
				}
			}else {
				JOptionPane.showMessageDialog(null, "性别输入有误: 男/女");
			}
		}
	}

	/**
	 * 鼠标右键菜单出现位置
	 */
	private void maybeShowPopup(MouseEvent e) {
		if (e.isPopupTrigger()) {
			popup.show(e.getComponent(), e.getX(), e.getY());
		}
	}

	/**
	 * 添加按钮事件
	 */
	private class AddStaffListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			SwingCreate.createFrame(new AddStaffFrame(), 550, 550, "添加员工");
		}
	}

	/**
	 * 刷新数据
	 */
	private class refeshStaffListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			tableModel.setDataVector(getStaffData(), tableTitle);
			CommFrameUtil.setDefaultTabelRow(jt, tableModel);
			System.out.println("刷新数据");
		}
	}

	// 获取员工数据
	public Object[][] getStaffData() {
		staffList = sDao.getAllStaff();
		Object[][] data = new Object[staffList.size()][tableTitle.length];
		int i = 0;
		for (Staff s : staffList) {
			data[i][0] = (i + 1);
			data[i][1] = s.getId();
			data[i][2] = s.getLoginNum();
			data[i][3] = s.getLoginPwd();
			data[i][4] = s.getName();
			data[i][5] = s.getAge();
			data[i][6] = s.getSex();
			data[i][7] = s.getPhone();
			data[i][8] = s.getIdcard();
			data[i][9] = s.getEntry();
			data[i][10] = s.getNote();
			i++;
		}
		return data;
	}

}
