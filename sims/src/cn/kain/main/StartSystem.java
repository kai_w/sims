package cn.kain.main;

import cn.kain.frame.LoginFrame;
import cn.kain.frame.SwingCreate;

/**
 * @author kai
 * @email kain.wong@foxmail.com
 * @Data 2020年1月3日 下午6:36:14
 * @Description 系统启动入口
 */
public class StartSystem {

	public static void main(String[] args) {
		SwingCreate.createFrame(new LoginFrame(), 500, 350, "系统登录");
	}

}
