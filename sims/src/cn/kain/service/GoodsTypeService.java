package cn.kain.service;

import cn.kain.entity.GoodsType;

/**
 * @author kai
 * @email kain.wong@foxmail.com
 * @Data 2020年1月8日 下午8:10:44
 * @Description TODO
 * 
 */
public interface GoodsTypeService {
	
	/**
	 * 添加商品类型
	 * @param type
	 */
	void addGoodsType(String typeName,String secodName,String note);
	
	
}
