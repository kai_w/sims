package cn.kain.service;

import cn.kain.entity.Staff;

/**
 * @author kai
 * @email kain.wong@foxmail.com
 * @Data 2020年1月12日 上午12:02:40
 * @Description TODO
 * 
 */
public interface StaffService {

	/**
	 * 添加员工
	 */
	public void addStaff(Staff s);
}
