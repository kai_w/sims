package cn.kain.service.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cn.kain.dao.GoodsTypeDao;
import cn.kain.dao.impl.GoodsTypeDaoImpl;
import cn.kain.entity.GoodsType;
import cn.kain.service.GoodsTypeService;

/**
 * @author kai
 * @email kain.wong@foxmail.com
 * @Data 2020年1月8日 下午8:11:25
 * @Description TODO
 * 
 */
public class GoodsTypeServiceImpl implements GoodsTypeService {

	private GoodsTypeDao typeDao = new GoodsTypeDaoImpl();

	@Override
	public void addGoodsType(String typeName, String secodName, String note) {
		GoodsType goodsType = new GoodsType();

		// 先判断一级目录是否已存在数据库中，
		List<GoodsType> typeList = typeDao.getAllType(0);
		// 获取所有的商品类型进行遍历，若有一个存在即结束遍历，因为数据量量少使用遍历，若数据量大还是在dao层数据库拉取数据
		int typeId = jungeTypeName(typeName);
		if (typeId!=0) {
			// 若存在
			goodsType.setTypeId(typeId);
			System.out.println("存在该类型");
		} else {
			// 若不存在，则需要创建新的typeid: 获取最大的typeId，然后加1
			goodsType.setTypeId(getMaxTypeId(typeList) + 1);
		}
		goodsType.setTypeName(typeName);
		goodsType.setTypeNameDetail(secodName);
		goodsType.setNote(note);
		typeDao.addType(goodsType);
	}

	/**
	 * 判断类型名是否存在，存在则返回typeid
	 * @param str
	 * @return
	 */
	public int jungeTypeName(String str) {
		List<GoodsType> typeList = typeDao.getAllType(0);
		for (GoodsType g : typeList) {
			if (str.equals(g.getTypeName())) {
				return g.getTypeId();
			}
		}
		return 0;
	}

	/**
	 * 获取集合中最大的type
	 * @param list
	 * @return
	 */
	public int getMaxTypeId(List<GoodsType> list) {
		// 使用集合的排序方法，按照typeId倒序排列
		Collections.sort(list, new Comparator<GoodsType>() {
			@Override
			public int compare(GoodsType g1, GoodsType g2) {
				return g2.getTypeId() - g1.getTypeId();
			}
		});
		return list.get(0).getTypeId(); // 获取集合中第一个的typeid
	}
}
