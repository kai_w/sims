package cn.kain.service.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cn.kain.dao.StaffDao;
import cn.kain.dao.impl.StaffDaoImpl;
import cn.kain.entity.Staff;
import cn.kain.service.StaffService;

/**
 * @author kai
 * @email kain.wong@foxmail.com
 * @Data 2020年1月12日 上午12:03:49
 * @Description TODO
 * 
 */
public class StaffServiceImpl implements StaffService {

	private static StaffDao sDao = new StaffDaoImpl();

	@Override
	public void addStaff(Staff s) {
		Staff staff = new Staff();
		staff.setLoginNum(produceLoginNum());
		staff.setLoginPwd("123456");  //设置默认密码为123456
		staff.setName(s.getName());
		staff.setAge(s.getAge());
		staff.setSex(s.getSex());
		staff.setPhone(s.getPhone());
		staff.setIdcard(s.getIdcard());
		staff.setEntry(s.getEntry());
		staff.setNote(s.getNote());
		sDao.addStaff(staff);
	}

	/**
	 * 产生一个登录账号
	 * 
	 * @return
	 */
	public  String produceLoginNum() {
		List<Staff> list = sDao.getAllStaff();
		Collections.sort(list, new Comparator<Staff>() {
			@Override
			public int compare(Staff s1, Staff s2) {
				return Integer.valueOf(s2.getLoginNum())-Integer.valueOf(s1.getLoginNum());
			}
		});
		return String.valueOf(Integer.valueOf(list.get(0).getLoginNum())+1);
	}
	
}
