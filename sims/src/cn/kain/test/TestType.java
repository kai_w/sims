package cn.kain.test;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.Test;

import cn.kain.dao.GoodsTypeDao;
import cn.kain.dao.impl.GoodsTypeDaoImpl;
import cn.kain.entity.GoodsType;
import cn.kain.service.GoodsTypeService;
import cn.kain.service.impl.GoodsTypeServiceImpl;

/**
 * @author kai
 * @email kain.wong@foxmail.com
 * @Data 2020年1月7日 下午6:13:45
 * @Description TODO
 * 
 */
public class TestType {

	@Test
	public void typeList() {
		GoodsTypeDao typeDao = new GoodsTypeDaoImpl();
		List<GoodsType> list = typeDao.getAllType(0);
		Collections.sort(list, new Comparator<GoodsType>() {
			@Override
			public int compare(GoodsType g1, GoodsType g2) {
				return g2.getTypeId()-g1.getTypeId();
			}
		});
		System.out.println(list.get(0).getTypeId());
		for(GoodsType t:list) {
			System.out.println(t);
		}
	}
	
	@Test
	public void judge() {
		GoodsTypeService s = new GoodsTypeServiceImpl();
		s.addGoodsType("食品类", "AAA", "");
	}
	
	@Test
	public void test2() {
		
		
		
		
	}
	
}
