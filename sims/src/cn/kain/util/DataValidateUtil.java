package cn.kain.util;

/**
 * @author kai
 * @email kain.wong@foxmail.com
 * @Data 2020年1月11日 下午10:46:49
 * @Description 数据校验工具
 * 
 */
public class DataValidateUtil {

	private static String intRegex = "^\\d{2}$";  //匹配两位数字
	private static String phoneRegex = "^1(3|5|6|8)[0-9]{9}$";  //匹配手机号 (手机号必须是：13,15,16,18开头)
	private static String idcardRegex = "\\d{17}(\\d|X|x)";  //通用18位身份证号
	
	
	/**
	 * 验证是否属于两个整数范围的整数
	 * (可用于年龄校验)
	 * @param min
	 * @param max
	 * @param num
	 * @return
	 */
	public static boolean validateInt(int min,int max,String numStr) {
		return numStr.matches(intRegex)?((Integer.valueOf(numStr)>max||Integer.valueOf(numStr)<min)?false:true):false;
	}
	
	/**
	 * 验证手机号 (手机号必须是：13,15,16,18开头的11位手机号)
	 * @param phone
	 * @return
	 */
	public static boolean validataPhone(String phone) {
		return phone.matches(phoneRegex);
	}
	
	/**
	 * 验证身份证号(使用通用18位身份证号)
	 * @param args
	 */
	public static boolean validataIdcard(String idcard) {
		return idcard.matches(idcardRegex);
	}
	
}
